
$(document).ready(function() {
 console.log( "ready!" );
 $('.slider').slick({
         infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
              breakpoint: 520,
              settings: {
                slidesToShow: 1,
              }
            }
        ]
    });
}); 